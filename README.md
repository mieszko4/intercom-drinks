# intercom-drinks
> 

## Task description

We have some customer records in a text file (customers.json, attached) one customer per line, JSON-encoded. We want to invite any customer within 100km of our Dublin office (GPS coordinates 53.3381985, -6.2592576) to some food and drinks on us.
Write a program that will read the full list of customers and output the names and user ids of matching customers (within 100km), sorted by user id (ascending).
You can use the first formula from this Wikipedia article to calculate distance: https://en.wikipedia.org/wiki/Great-circle_distance -- don't forget, you'll need to convert degrees to radians. Your program should be fully tested too.

## Requirements

* ```git```
* ```npm``` and ```node.js```

## Install

```sh
$ npm install bitbucket:mieszko4/intercom-drinks
```


## Usage

```js
var intercomDrinks = require('intercom-drinks');
var radius = 100000; //in meters

intercomDrinks.parseCustomers('https://gist.githubusercontent.com/brianw/19896c50afa89ad4dec3/raw/6c11047887a03483c50017c1d451667fd62a53ca/gistfile1.txt', function (customers) {
  var nearbyCustomers = intercomDrinks.getNearbyCustomers(53.3381985, -6.2592576, radius, customers);
  console.log(nearbyCustomers.map(intercomDrinks.printCustomer).join('\n'));
});
```

## Stream example

```js
var SortedArray = require('sorted-array');
var ThrottleStream = require('m-throttle-stream');
var JsonStream = require('m-json-stream');
var intercomDrinks = require('intercom-drinks');
var request = require('request');

//define parameters
var radius = 100000; //in meters
var latitude = 53.3381985;
var longitude = -6.2592576;

var source = request('https://gist.githubusercontent.com/brianw/19896c50afa89ad4dec3/raw/6c11047887a03483c50017c1d451667fd62a53ca/gistfile1.txt');
var destination = source
  //simulate throttle
  .pipe(new ThrottleStream({
    maxTimeout: 100,
    bufferSize: 10000 //should be higher than chunks comming in
  }))
  //parse each line, ignore non-objects
  .pipe(new JsonStream({strict: false}))
;

//prepare result array for insert sort
var comparatorFunction = function (a, b) {
  return a.user_id - b.user_id; //ascending
};
var nearbyCustomers = new SortedArray([], comparatorFunction);

//on each row representing object
destination.on('data', function (row) {
  var customer;

  try { //validate, ignore non-customers
    customer = intercomDrinks.validateCustomer(row);
  } catch (e) {
    console.warn(row, ' is not customer object');
    return;
  }

  //check if nearby customer
  if (intercomDrinks.isNearbyCustomer(latitude, longitude, radius, customer)) {
    console.log('pushing', customer);
    nearbyCustomers.insert(customer); //insert to sorted array
  }
});

destination.on('end', function () {
  console.log('total pushed', nearbyCustomers.array.length);
  console.log(nearbyCustomers.array.map(intercomDrinks.printCustomer).join('\n'));
});
```

## Development


```sh
$ git clone https://mieszko4@bitbucket.org/mieszko4/intercom-drinks.git
$ cd intercom-drinks
$ npm install
$ npm test
```

# TODO

* Asynchronous calls
* Customer class


## License

Apache-2.0 © [Mieszko4]()