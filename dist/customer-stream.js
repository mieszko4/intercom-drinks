'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _core = require('./core');

var _core2 = _interopRequireDefault(_core);

var _stream = require('stream');

var _util = require('util');

var _util2 = _interopRequireDefault(_util);

// Stream that expects stream of customer objects
function CustomerStream() {
  var _ref = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];

  var _ref$latitude = _ref.latitude;
  var latitude = _ref$latitude === undefined ? 0 : _ref$latitude;
  var _ref$longitude = _ref.longitude;
  var longitude = _ref$longitude === undefined ? 0 : _ref$longitude;
  var _ref$radius = _ref.radius;
  var radius = _ref$radius === undefined ? 0 : _ref$radius;
  var _ref$strict = _ref.strict;
  var strict = _ref$strict === undefined ? false : _ref$strict;

  if (!(this instanceof CustomerStream)) {
    return new CustomerStream();
  }

  _stream.Transform.call(this, {
    objectMode: true,
    readableObjectMode: true,
    writableObjectMode: true
  });

  this._latitude = latitude;
  this._longitude = longitude;
  this._radius = radius;
  this._strict = strict;
}

_util2['default'].inherits(CustomerStream, _stream.Transform);

CustomerStream.prototype._transform = function (chunk, encoding, done) {
  var customer = undefined;
  try {
    customer = _core2['default'].validateCustomer(chunk);
  } catch (e) {
    if (this._strict) {
      this.emit('error', new Error(_core.exceptions.INVALID_CONTENT));
    } else {
      done();
    }

    return;
  }

  if (_core2['default'].isNearbyCustomer(this._latitude, this._longitude, this._radius, customer)) {
    this.push(customer);
  }
  done();
};

exports['default'] = CustomerStream;
module.exports = exports['default'];
//degrees
//degrees
//meters