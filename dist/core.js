'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _request = require('request');

var _request2 = _interopRequireDefault(_request);

var getRadiusMark = function getRadiusMark(radius) {
  return Math.cos(radius / module.exports.R); //used for skipping sine operations
};

var R = 6371009;
exports.R = R;
var exceptions = {
  INVALID_CONTENT: 'Invalid Content'
};

exports.exceptions = exceptions;
exports['default'] = {
  comparatorFunction: function comparatorFunction(a, b) {
    return a.user_id - b.user_id; //ascending
  },

  // Calculates distance using estimated formula for spherical Earth projected to a plane
  getGeographicalDistance: function getGeographicalDistance(originLat, originLng, destinationLat, destinationLng) {
    var distanceMark = module.exports['default'].getGeographicalDistanceMark(originLat, originLng, destinationLat, destinationLng);
    var distance = module.exports.R * Math.acos(distanceMark);
    return distance;
  },

  // Calculates distance mark (without square root and not multiplied by R)
  getGeographicalDistanceMark: function getGeographicalDistanceMark(originLat, originLng, destinationLat, destinationLng) {
    var degreeToRadian = Math.PI / 180;

    //tranform to radian
    originLat *= degreeToRadian;
    originLng *= degreeToRadian;
    destinationLat *= degreeToRadian;
    destinationLng *= degreeToRadian;

    var deltaLng = Math.abs(originLng - destinationLng);

    var distanceMark = Math.sin(originLat) * Math.sin(destinationLat) + Math.cos(originLat) * Math.cos(destinationLat) * Math.cos(deltaLng);
    return distanceMark;
  },

  // Parses customer string
  parseCustomer: function parseCustomer(customerString) {
    var customer = undefined;

    try {
      customer = JSON.parse(customerString);
    } catch (e) {
      throw module.exports.exceptions.INVALID_CONTENT;
    }

    return customer;
  },

  // Checks whether customer object is valid
  validateCustomer: function validateCustomer(object) {
    var customerKeys = ['user_id', 'name', 'latitude', 'longitude'];

    //simple validation of customer fields
    customerKeys.forEach(function (key) {
      if (!(key in object)) {
        throw module.exports.exceptions.INVALID_CONTENT;
      }
    });

    return object;
  },

  // Returns array of customer objects
  parseCustomers: function parseCustomers(filename, callback) {
    (function (process) {
      _fs2['default'].exists(filename, function (exists) {
        if (exists) {
          _fs2['default'].readFile(filename, 'utf8', function (error, data) {
            if (error) {
              throw module.exports.exceptions.INVALID_CONTENT;
            }
            process(data);
          });
        } else {
          //try url
          (0, _request2['default'])(filename, function (error, response, body) {
            if (error) {
              throw module.exports.exceptions.INVALID_CONTENT;
            }

            if (response.statusCode === 200) {
              process(body);
            }
          });
        }
      });
    })(function (data) {
      //this is definition of process callback
      var customers = data.trim().split('\n').map(function (customerString) {
        return module.exports['default'].validateCustomer(module.exports['default'].parseCustomer(customerString));
      });
      callback(customers);
    });
  },

  // Checks if customer is near by
  isNearbyCustomer: function isNearbyCustomer(lat, lng, radius, customer) {
    var radiusMark = getRadiusMark(radius);
    var distanceMark = module.exports['default'].getGeographicalDistanceMark(lat, lng, customer.latitude, customer.longitude);

    return distanceMark > radiusMark; //inverse because of cosine
  },

  // Returns sorted by id customers that are in within radius of (lat, lng)
  getNearbyCustomers: function getNearbyCustomers(lat, lng, radius, customers) {
    var radiusMark = getRadiusMark(radius);
    var nearbyCustomers = customers.filter(function (customer) {
      var distanceMark = module.exports['default'].getGeographicalDistanceMark(lat, lng, customer.latitude, customer.longitude);

      return distanceMark > radiusMark; //inverse because of cosine
    }).sort(module.exports['default'].comparatorFunction);

    return nearbyCustomers;
  },

  // Print customer
  printCustomer: function printCustomer(customer) {
    return customer.user_id + ':' + customer.name;
  }
};