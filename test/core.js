import assert from 'assert';
import intercomDrinks from '../lib';
import {exceptions as intercomExceptions} from '../lib';

describe('intercom-drinks core', function () {
  it('should give distance of 0 for the same origin and destination', function () {
    let lat = 53.3381985;
    let lng = -6.2592576;

    assert.equal(0, intercomDrinks.getGeographicalDistance(lat, lng, lat, lng));
  });

  it('should give good estimate for two very close points', function () {
    let expectedDistance = 491;
    let actualDistance = intercomDrinks.getGeographicalDistance(45.806701, 15.970511, 45.803350, 15.974631);

    assert(Math.abs(expectedDistance - actualDistance) < 10);
  });

  it('should give good estimate for two close points', function () {
    let expectedDistance = 15228;
    let actualDistance = intercomDrinks.getGeographicalDistance(45.797083, 15.886604, 45.804485, 16.082202);

    assert(Math.abs(expectedDistance - actualDistance) < 100);
  });

  it('should give good estimate for two distant points', function () {
    let expectedDistance = 126604;
    let actualDistance = intercomDrinks.getGeographicalDistance(45.797083, 15.886604, 46.737494, 14.960032);

    assert(Math.abs(expectedDistance - actualDistance) < 1000);
  });

  it('should give good estimate for two very distant points', function () {
    let expectedDistance = 506073;
    let actualDistance = intercomDrinks.getGeographicalDistance(45.797083, 15.886604, 50.283393, 14.741687);

    assert(Math.abs(expectedDistance - actualDistance) < 1000);
  });

  it('should parse complete customer string', function () {
    let expectedCustomer = {
      user_id: 1,
      name: 'A',
      latitude: 10,
      longitude: 20
    };
    let actualCustomer = intercomDrinks.validateCustomer(intercomDrinks.parseCustomer(`
      {
        "user_id": 1,
        "name": "A",
        "latitude": 10,
        "longitude": 20
      }
    `));

    assert.deepEqual(expectedCustomer, actualCustomer);
  });

  it('should not parse customer string with missing attributes', function () {
    assert.throws(
      function () {
        intercomDrinks.validateCustomer(intercomDrinks.parseCustomer('{"user_id": 1}'));
      },
      intercomExceptions.INVALID_CONTENT
    );
  });

  it('should not parse invalid customer string', function () {
    assert.throws(
      function () {
        intercomDrinks.validateCustomer(intercomDrinks.parseCustomer('{"user_id": 1'));
      },
      intercomExceptions.INVALID_CONTENT
    );
  });

  it('should parse file with customers', function (done) {
    intercomDrinks.parseCustomers('./test/fixtures/customers.json', function () {
      done();
    });
  });

  it('should print customer', function () {
    var customer = {
      user_id: 1,
      name: 'Miłosz',
      latitude: 10,
      longitude: 20
    };
    assert.equal('1:Miłosz', intercomDrinks.printCustomer(customer));
  });

  it('should positively check nearby customer', function () {
    var customer = {
      user_id: 3,
      name: 'Bariša',
      latitude: 53.3381985,
      longitude: -7.2592576
    };

    assert(intercomDrinks.isNearbyCustomer(53.3381985, -6.2592576, 100000, customer));
  });

  it('should negatively check nearby customer', function () {
    var customer = {
      user_id: 8,
      name: 'Basia',
      latitude: 3.3381985,
      longitude: -6.2592576
    };

    assert(!intercomDrinks.isNearbyCustomer(53.3381985, -6.2592576, 100000, customer));
  });

  it('should filter and print out nearby customers from file', function (done) {
    intercomDrinks.parseCustomers('./test/fixtures/customers.json', function (customers) {
      let nearbyCustomers = intercomDrinks.getNearbyCustomers(53.3381985, -6.2592576, 100000, customers);

      let expectedOutput = [
        '1:Miłosz',
        '3:Bariša',
        '6:Sara'
      ].join('\n');
      let actualOutput = nearbyCustomers.map(intercomDrinks.printCustomer).join('\n');

      assert.equal(expectedOutput, actualOutput);
      done();
    });
  });

  it('should filter and print out nearby customers from url', function (done) {
    let url = 'https://gist.githubusercontent.com/brianw/19896c50afa89ad4dec3/raw/6c11047887a03483c50017c1d451667fd62a53ca/gistfile1.txt';
    intercomDrinks.parseCustomers(url, function (customers) {
      let nearbyCustomers = intercomDrinks.getNearbyCustomers(53.3381985, -6.2592576, 100000, customers);

      let expectedOutput = [
        '4:Ian Kehoe',
        '5:Nora Dempsey',
        '6:Theresa Enright',
        '8:Eoin Ahearn',
        '11:Richard Finnegan',
        '12:Christina McArdle',
        '13:Olive Ahearn',
        '15:Michael Ahearn',
        '17:Patricia Cahill',
        '23:Eoin Gallagher',
        '24:Rose Enright',
        '26:Stephen McArdle',
        '29:Oliver Ahearn',
        '30:Nick Enright',
        '31:Alan Behan',
        '39:Lisa Ahearn'
      ].join('\n');
      let actualOutput = nearbyCustomers.map(intercomDrinks.printCustomer).join('\n');

      assert.equal(expectedOutput, actualOutput);
      done();
    });
  });

});
