import assert from 'assert';
import fs from 'fs';
import JsonStream from 'm-json-stream';
import intercomDrinks from '../lib';
import {CustomerStream, exceptions as intercomExceptions} from '../lib';

describe('intercom-drinks customer-stream', function () {
  it('should filter and print out nearby customers from readable', function (done) {
    let expectedOutput = [
      '1:Miłosz',
      '3:Bariša',
      '6:Sara'
    ].join('\n');

    var source = fs.createReadStream('./test/fixtures/customers.json');
    var destination = source
      .pipe(new JsonStream())
      .pipe(new CustomerStream({
        latitude: 53.3381985,
        longitude: -6.2592576,
        radius: 100000
      }))
    ;

    var nearbyCustomers = [];
    destination.on('data', function (row) {
      nearbyCustomers.push(row);
    });

    destination.on('end', function () {
      let actualOutput = nearbyCustomers.map(intercomDrinks.printCustomer).join('\n');

      assert.equal(expectedOutput, actualOutput);
      done();
    });
  });

  it('should filter nearby customers from readable with errornous data (non-strict)', function (done) {
    let expectedOutput = [
      '1:Miłosz',
      '6:Sara'
    ].join('\n');

    var source = fs.createReadStream('./test/fixtures/customers-invalid.json');
    var destination = source
      .pipe(new JsonStream())
      .pipe(new CustomerStream({
        latitude: 53.3381985,
        longitude: -6.2592576,
        radius: 100000,
        strict: false
      }))
    ;

    var nearbyCustomers = [];
    destination.on('data', function (row) {
      nearbyCustomers.push(row);
    });

    destination.on('end', function () {
      let actualOutput = nearbyCustomers.map(intercomDrinks.printCustomer).join('\n');

      assert.equal(expectedOutput, actualOutput);
      done();
    });
  });

  it('should filter nearby customers from readable with errornous data (non-strict)', function (done) {
    var source = fs.createReadStream('./test/fixtures/customers-invalid.json');
    var destination = source
      .pipe(new JsonStream())
      .pipe(new CustomerStream({
        latitude: 53.3381985,
        longitude: -6.2592576,
        radius: 100000,
        strict: true
      }))
    ;

    destination.on('error', function (error) {
      assert.equal(intercomExceptions.INVALID_CONTENT, error.message);
      done();
    });
  });


});
