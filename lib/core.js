import fs from 'fs';
import request from 'request';

var getRadiusMark = function (radius) {
  return Math.cos(radius / module.exports.R);  //used for skipping sine operations
};

export const R = 6371009;
export const exceptions = {
  INVALID_CONTENT: 'Invalid Content'
};

export default {
  comparatorFunction: function (a, b) {
    return a.user_id - b.user_id; //ascending
  },

  // Calculates distance using estimated formula for spherical Earth projected to a plane
  getGeographicalDistance: function (originLat, originLng, destinationLat, destinationLng) {
    let distanceMark = module.exports.default.getGeographicalDistanceMark(originLat, originLng, destinationLat, destinationLng);
    let distance = module.exports.R * Math.acos(distanceMark);
    return distance;
  },

  // Calculates distance mark (without square root and not multiplied by R)
  getGeographicalDistanceMark: function (originLat, originLng, destinationLat, destinationLng) {
    let degreeToRadian = Math.PI / 180;

    //tranform to radian
    originLat *= degreeToRadian;
    originLng *= degreeToRadian;
    destinationLat *= degreeToRadian;
    destinationLng *= degreeToRadian;

    let deltaLng = Math.abs(originLng - destinationLng);

    let distanceMark = Math.sin(originLat) * Math.sin(destinationLat)
      + Math.cos(originLat) * Math.cos(destinationLat) * Math.cos(deltaLng);
    return distanceMark;
  },

  // Parses customer string
  parseCustomer: function (customerString) {
    let customer;

    try {
      customer = JSON.parse(customerString);
    } catch (e) {
      throw module.exports.exceptions.INVALID_CONTENT;
    }

    return customer;
  },

  // Checks whether customer object is valid
  validateCustomer: function (object) {
    let customerKeys = [
      'user_id',
      'name',
      'latitude',
      'longitude'
    ];

    //simple validation of customer fields
    customerKeys.forEach(function (key) {
      if (!(key in object)) {
        throw module.exports.exceptions.INVALID_CONTENT;
      }
    });

    return object;
  },

  // Returns array of customer objects
  parseCustomers: function (filename, callback) {
    (function (process) {
      fs.exists(filename, function (exists) {
        if (exists) {
          fs.readFile(filename, 'utf8', function (error, data) {
            if (error) {
              throw module.exports.exceptions.INVALID_CONTENT;
            }
            process(data);
          });
        } else { //try url
          request(filename, function (error, response, body) {
            if (error) {
              throw module.exports.exceptions.INVALID_CONTENT;
            }

            if (response.statusCode === 200) {
              process(body);
            }
          });
        }
      });
    }(function (data) { //this is definition of process callback
      let customers = data.trim().split('\n').map(function (customerString) {
        return module.exports.default.validateCustomer(module.exports.default.parseCustomer(customerString));
      });
      callback(customers);
    }));
  },

  // Checks if customer is near by
  isNearbyCustomer: function (lat, lng, radius, customer) {
    let radiusMark = getRadiusMark(radius);
    let distanceMark = module.exports.default.getGeographicalDistanceMark(lat, lng, customer.latitude, customer.longitude);

    return distanceMark > radiusMark; //inverse because of cosine
  },

  // Returns sorted by id customers that are in within radius of (lat, lng)
  getNearbyCustomers: function (lat, lng, radius, customers) {
    let radiusMark = getRadiusMark(radius);
    let nearbyCustomers = customers.filter(function (customer) {
      let distanceMark = module.exports.default.getGeographicalDistanceMark(lat, lng, customer.latitude, customer.longitude);

      return distanceMark > radiusMark; //inverse because of cosine
    }).sort(module.exports.default.comparatorFunction);

    return nearbyCustomers;
  },

  // Print customer
  printCustomer: function (customer) {
    return customer.user_id + ':' + customer.name;
  }
};
