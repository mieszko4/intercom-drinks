import core from './core';
import {exceptions} from './core';
import {Transform} from 'stream';
import util from 'util';

// Stream that expects stream of customer objects
function CustomerStream({
  latitude: latitude = 0, //degrees
  longitude: longitude = 0, //degrees
  radius: radius = 0, //meters
  strict: strict = false
} = {}) {
  if (!(this instanceof CustomerStream)) {
    return new CustomerStream();
  }

  Transform.call(this, {
    objectMode: true,
    readableObjectMode: true,
    writableObjectMode: true
  });

  this._latitude = latitude;
  this._longitude = longitude;
  this._radius = radius;
  this._strict = strict;
}

util.inherits(CustomerStream, Transform);

CustomerStream.prototype._transform = function (chunk, encoding, done) {
  let customer;
  try {
    customer = core.validateCustomer(chunk);
  } catch (e) {
    if (this._strict) {
      this.emit('error', new Error(exceptions.INVALID_CONTENT));
    } else {
      done();
    }

    return;
  }

  if (core.isNearbyCustomer(this._latitude, this._longitude, this._radius, customer)) {
    this.push(customer);
  }
  done();
};

export default CustomerStream;
